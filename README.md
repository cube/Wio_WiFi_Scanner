# README #
Seeed Wio Terminal - scan for and display up to 10 access points on the display.

A modification of the Wifi Scan example that removes the Serial output so that the Seeed Wio Terminal can be plugged in anywhere and will display nearby access points within range on the built in display. 

Strong APs are shown in green, medium strength in orange, and weak signals in red. A * indicates whether an AP is encrypted or not. 