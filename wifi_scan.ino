#include "rpcWiFi.h"
#include "TFT_eSPI.h"

TFT_eSPI tft;

void setup()
{
  // Set up the display
  tft.begin();
  tft.setRotation(3);
  tft.fillScreen(TFT_BLACK);
  tft.setFreeFont(&FreeSans9pt7b);
  tft.fillScreen(TFT_BLACK);

  // Intro messages
  tft.setTextColor(TFT_WHITE);
  tft.drawString("Wi-Fi Scanner", 10, 10);
  tft.setTextColor(TFT_RED);
  tft.drawString("Scanning...", 10, 30);

  // Set up WiFi
  WiFi.mode(WIFI_STA);
  WiFi.disconnect(); // Ensure we are not connected to an AP
}

void loop()
{
  // Scan for networks and create array of seen SSIDs
  int number_of_networks = WiFi.scanNetworks();
  String seen_ssids[number_of_networks];

  // Reset the screen to it's title only to prepare to display APs
  tft.fillScreen(TFT_BLACK);
  tft.setTextColor(TFT_WHITE);
  tft.drawString("Wi-Fi Networks:", 10, 10);

  // Text positioning (starting at 10)
  int textpos = 30;
  int seen_ptr = 0;

  // Loop through networks
  for(int i = 0; i < number_of_networks; i++)
  {
    // Check if we already saw that SSID, we don't have much
    // screen space, so there's no point showing repeats
    bool seen = false;
    for(int x = 0; x < number_of_networks; x++)
    {
      if(WiFi.SSID(i) == seen_ssids[x])
      {
        seen = true;
        break;
      }
    }

    // As long as it isn't already displayed, continue
    if(!seen && WiFi.SSID(i)!="")
    {
      // Add a "*" to the start of the SSID if it is locked
      String enc_string = "* ";
      if(WiFi.encryptionType(i) == WIFI_AUTH_OPEN)
      {
        enc_string = "";
      }
      String text = enc_string + WiFi.SSID(i);

      // Determine the font color based on signal strength
      if(WiFi.RSSI(i) > -61){tft.setTextColor(TFT_GREEN);}
      else if(WiFi.RSSI(i) < -60 && WiFi.RSSI(i) > -91){tft.setTextColor(TFT_ORANGE);}
      else{tft.setTextColor(TFT_RED);}

      // Print the SSID to screen
      tft.drawString(text, 10, textpos);

      // Adjust necessary counters
      textpos += 20;
      seen_ssids[seen_ptr] = WiFi.SSID(i);
      seen_ptr++;
    }
  }
  delay(5000);
}
